jQuery(document).ready(function() {
	// Get the matches already saved in the database
	document.cookie = "TAN_update=; path=/";
	var savedMatches = php_data.savedMatches;
	var addToCookie = {};
	var totalMatchedKeywords = 0;

	// replacement function, is given a text node as element and a target to replace
	var tan_replaceFunc = function(element) {
		var merchant = element.attr('data-adv');
		var keyword = element.text();

		function tan_findAndReplace(data){
			for(var i=0 ; i<data.length ; i++)
			{
				if(merchant == parseInt(data[i].adv_id))
				{
					var link = "http://traveladsnet.go2cloud.org/aff_c?offer_id="+data[i].off_id+"&aff_id="+php_data.affiliate_id+"&url="+encodeURIComponent(data[i].offer_url+encodeURIComponent(data[i].advertiser_city_id));
					element.replaceWith("<a target='_blank' class=tanDeepLink href='"+link+"'>"+keyword+"</a>");
				}
			}
		}

		if(typeof savedMatches[keyword.toLowerCase()] === 'undefined' || savedMatches[keyword.toLowerCase()] === null)
		{
			// call to our api, to access ONLY if no match was found in the affiliate's database
			savedMatches[keyword.toLowerCase()] = {"notYet": true};
			jQuery.ajax({
				url: "https://traveladsnetwork.com/api-from-js/?method=getOffers&search="+keyword+"&aff_id="+php_data.affiliate_id+"&onlyTargeted=true",
				dataType: 'json',
				success: function(data) {
					var tmp = [];
					for(var j=0 ; j<data.length ; j++)
						if(typeof data[j].advertiser_city_id != 'undefined')
							tmp.push({"off_id": data[j].off_id, 
									"offer_url": data[j].offer_url,
									"adv_id": data[j].advertiser_id,
									"advertiser_city_id": data[j].advertiser_city_id});
					data=tmp.slice(0, 10);
					savedMatches[keyword.toLowerCase()] = data;
					addToCookie[keyword.toLowerCase()] = data;
					document.cookie = 'TAN_matches='+JSON.stringify(addToCookie)+'; path=/';
					tan_findAndReplace(data);
				}
			});
		}
		else
		{
			var stop = setInterval(function(){
				if(typeof savedMatches[keyword.toLowerCase()].notYet == 'undefined')
				{
					tan_findAndReplace(savedMatches[keyword.toLowerCase()]);
					clearInterval(stop);
				}
			}, 200);
		}

	};

	// replace words on the content only
	jQuery("span.TAN_link").each(function(){
		tan_replaceFunc(jQuery(this))
	});
});			
