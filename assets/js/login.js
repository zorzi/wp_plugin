jQuery(document).ready(function($){
	$("#TANLoginForm").submit(function () {
		jQuery.ajax({
			type: 'POST',
			url: 'https://traveladsnetwork.com/api-remote-login/?method=checkCredentials',
			data: {
				username: $('input[name=username]').val(),
				password: $('input[name=password]').val()
			},
			success: function(data){
				if(data.status == 'success')
				{
					document.cookie = "TAN_login="+data.data.new_cookie+"; path=/";
					document.cookie = "TAN_hoID="+data.data.affiliate_id+"; path=/";
					document.cookie = "TAN_username="+$('input[name=username]').val()+"; path=/";
					location.reload();
				}
			},
			error: function(data){
				data=data.responseJSON;

				$('input[name=username], input[name=password]').addClass('animateWrongCreds');
				$('#wrongCreds .control').text(data.message);
				setTimeout(function() {
					$('input[name=username], input[name=password]').removeClass('animateWrongCreds');
				}, 300*3);
				$('#wrongCreds').toggle(true);
			}
		});
		return false;
	});
});
