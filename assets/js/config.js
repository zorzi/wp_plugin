jQuery(document).ready(function($) {
	$('#searchMerchants').on('keyup change', function () {
		var search = new RegExp($(this).val(), 'i');
		$('ul#merchantList li').each(function () {
			var val = $(this).text();
			$(this).toggle( !! val.match(search)).html(
				val.replace(search, function(match) {
					return '<mark>'+match+'</mark>'}, 'gi')
			);
		});
	});
	$('ul#merchantList li').click(function() {
		$(this).toggleClass('selected');
		if($(this).hasClass('selected'))
		{
			if($('ul#merchantList li.selected').length >= 10)
			{
				$(this).toggleClass('selected');
				alert('10 merchants max');
				return;
			}
			$('ul#selectedMerchantList').append('<li value='+$(this).attr('value')+'> '
								+'<span class=iconCross>x</span>'
								+$(this).text())
								+'</li>';
			
			$('span.iconCross').click(function() {
				var father = $(this).parents('#selectedMerchantList > li');
				$('#merchantList > li[value='+father.attr('value')+']').removeClass('selected');
				father.remove();
			});
			$('.iconCross').toggle(false);
			$('.iconCross').toggle(true);
		}
		else
			$('ul#selectedMerchantList li[value='+$(this).attr('value')+']').remove();
	});
	$('ul#selectedMerchantList').sortable({ 
		accept: '*',
		activeClass: '',
		cancel: 'input, textarea, button, select, option, span',
		connectWith: false,
		disabled: false,
		forcePlaceholderSize: true,
		handle: false,
		initialized: false,
		items: 'li, div',
		placeholder: 'placeholder',
		placeholderTag: null,
		updateHandler: null,
		receiveHandler: null})
			.on('sortable:activate', function(e, ui){
				$('.iconCross').toggle(false);
			})
			.on('sortable:beforeStop', function(e, ui){
				$('.iconCross').toggle(true);
			});
	$('#pageSearch').on('keyup change', function () {
		var search = new RegExp($(this).val(), 'i');
		$('#pagesList li').each(function () {
			var val = $(this).text();
			$(this).toggle( !! val.match(search)).html(
				val.replace(search, function(match) {
					return '<mark>'+match+'</mark>'}, 'gi')
			);
		});
	});
	$('#pagesList li').click(function() {
		$(this).toggleClass('selected');

	});
	$('#pageSelectAll').click(function() {
		$('#pagesList li:not(.selected)').each(function(){
			if($(this).css('display') != 'none')
				$(this).addClass('selected');
		});
	});
	$('#pageSelectNone').click(function() {
		$('#pagesList li.selected').each(function(){
			if($(this).css('display') != 'none')
				$(this).removeClass('selected');
		});
	});
	var soloAutoHeight = $('#soloAuto').height()+'px';
	$('#soloAuto').css('max-height', soloAutoHeight);
	$('input[type=radio][name=mode]').change(function(){
		if($('input[type=radio][name=mode]:checked').attr('value')=='manual')
		{
			$('#soloAuto').css('max-height', '0');
			$('#soloAuto').css('background', '#eee');
			$('.iconCross').toggle(false);
		}
		else
		{
			$('.iconCross').toggle(true);
			$('#soloAuto').css('max-height', soloAutoHeight);
			$('#soloAuto').css('background', '');
		}
	});

	$('button.saveChangesButton').click(function() {
		var yesNo = confirm('If you save, all previous configurations of the selected pages will be overwritten, are you sure?');
		if(!yesNo)
			return;

		var mode = $('#autoMode:checked').length;
		var maxLinks = $('input#linkAmount').val();
		var updateLinks = $('#yesUpdate:checked').length;
		
		var merchants = [];
		$('#selectedMerchantList li').each(function() {
			merchants.push($(this).attr('value'));
		});

		var postsIds = [];
		$('#pagesList ul > li.selected').each(function() {
			postsIds.push($(this).attr('value'));
		});

		var finalArr = {	pages: 	postsIds,
					mode: 	mode,
					linkAmount:	maxLinks,
					updateLinks:updateLinks,
					merchants:	merchants};

		document.cookie = "TAN_update="+JSON.stringify(finalArr)+"; path=/";
		document.cookie = "TAN_savedConfig="+JSON.stringify(finalArr)+"; path=/";
		location.reload();
	});


	savedConfig=JSON.parse(php_data);

	if(savedConfig !== null)
	{
		if(savedConfig.mode)
			$('input[name=mode][value=auto]').click();
		else
			$('input[name=mode][value=manual]').click();
		if(savedConfig.updateLinks)
			$('input[name=updateLinks][value=manual]').click();
		else
			$('input[name=updateLinks][value=auto]').click();
		$('#linkAmount').val(savedConfig.linkAmount);
		if(typeof savedConfig.merchants !== 'undefined')
			for(var i=0 ; i<savedConfig.merchants.length ; i++)
				$('#merchantList li[value='+savedConfig.merchants[i]+']').click();
		if(typeof savedConfig.pages !== 'undefined')
			for(var i=0 ; i<savedConfig.pages.length ; i++)
				$('#pagesList li[value='+savedConfig.pages[i]+']').click();
	}

});

