=== Traveladsnetwork ===
Contributors: christophesecher
Tags: travel, affiliate plugin, bloggers, merchant, travel content, travel blogs, monetize,
Requires at least: 4.9.0
Tested up to: 4.9.8
Requires PHP: 7.3
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Install the WordPress Travel Plugin on your website and help your audience find the cheapest flights, hotels and activities, while generating steady income.

== Description ==
#Description
Install the WordPress Travel Plugin on your website and help your audience find the cheapest flights, hotels and activities, while generating steady income.

__Note:__ It has nothing to do with advertising. We offer in-demand and useful services!
This WordPress Travel Plugin is a very good solution for travel bloggers, travel agencies and 
travel-related websites.

#What does the “Travel Ads Network” plugin can offer:

__For your website visitors__: helping your website visitors to find the best hotel, flight and activity offers according to their travel needs.

__For you__: a new source of income and added value to your website.

#How does it work?

Install and activate our plugin. You place our widgets right in your posts and help visitors to 
book favorable flights, hotels and other services - (note: Initial scope would be only hotels but 
with future extension).

Your website audience will be happy to grab a great deal and get to have an 
unforgettable journey, while we pay you a commission.

Your earnings will be displayed in your Travel Ads Network Dashboard and in your plugin. We make scheduled, automated, monthly payouts.

#What potential features will this plugin have?
We are considering building this additional features in a later phase:  
- Search Forms
- Hotel Widget

We value your comments and feedbacks.

Languages supported:
- English

== Installation ==
1. Upload the plugin to the `/wp-content/plugins/` directory
2. Activate the plugin through the \'Plugins\' menu in WordPress
3. __(Optionnal)__ configure the plugin in the TravelAds menu, in your wordpress site
4. Let the plugin do its work!

== Frequently Asked Questions ==
If any issues, please send an email to wp_plugin@traveladsnetwork.com

== Screenshots ==
1. /assets/screenshot1.png
2. /assets/screenshot2.png

== Changelog ==
= 1.0 =
- First version!

== Upgrade Notice ==
No upgrade available for now
