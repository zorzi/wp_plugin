<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// initialize
if ( is_admin() ) {
	add_action('add_meta_boxes', function () {
		$screens = array ('post', 'page');	
		$args = array (
			'public'		=> true,
			'_builtin'	=> false
		);
		$custom_post_types = get_post_types ($args, 'names', 'and');
		$screens = array_values (array_merge ($screens, $custom_post_types));
	
		foreach ($screens as $screen) {
			add_meta_box (
				'travelads_sectionid',
				'<img src="'.plugins_url("assets/images/logo.png", dirname(__FILE__)).'">'.' TravelAdsNetwork',
				'tan_add_meta_box_callback',
				$screen
			);
		}//End foreach
	});
	
}



function tan_add_meta_box_callback($post) {
		global $wpdb;

		$result = $wpdb->get_results("SELECT mode FROM wp_tan_pages_config WHERE page_id = '". $post->ID ."'");

		if (count($result) === 0 || $result[0]->{'mode'} !== "0")
			return;

		$code = tan_test_log();
		
		if($code !== 202){
			echo "<center><a href='admin.php?page=travelads'>Please Log In</a></center>";
		}else{
		
		$pageInfos = $wpdb->get_results("SELECT * FROM wp_tan_pages_config WHERE page_id = '". $post->ID ."'")[0];
		?>
			<div id=navMenuOffset> </div>
			<table id='navMenu'>
				<tr>
					<td class=navMenu_autoWidth>
						<input id=navMenu_keyWord type=text readonly=readonly>
					</td>
					<td id=navMenu_arrowsCell>
						<div>
							<span id=navMenuprevWord>&lt;</span>
							<span id=navMenunextWord>&gt;</span>
						</div>
					</td>
					<td>
						<span id=navMenu_currentKeyWord></span> of <span id=navMenu_totalKeyWord></span>
					</td>
					<td class=navMenu_autoWidth>
						Select merchant:
					</td>
					<td>
						<select id=navMenu_merchantSelect>
							<?php
								$allmerchants=json_decode(file_get_contents(plugins_url("data/advertisers.json", dirname(__FILE__))));

								if(!count(json_decode($pageInfos->{'merchants'})))
									foreach($allmerchants as $oneMerch)
										echo "<option value=".$oneMerch->{'id'}."> ".$oneMerch->{'name'};
								else
									foreach(json_decode($pageInfos->{'merchants'}) as $m)
										foreach($allmerchants as $oneMerch)
											if($oneMerch->{'id'} == intval($m))
											{
												echo "<option value=".$m."> ".$oneMerch->{'name'};
												break;
											}
							?>
						</select>
					</td>
					<td class=navMenu_autoWidth>
						Insert Link:
					</td>
					<td style='width:auto'>
						<div id='navMenu_buttonOnOff'></div>
					</td>
				</tr>
			</table>
		<?php

		echo "<div id='hidden_travel_editor' style='display: none'></div>";
		echo "<div id='travel_editor'></div>";

		}
}//End function

add_action('save_post', function ($post_id) {
	global $wpdb;

	$post = get_post($post_id);
	if($post->post_type === "page" or $post->post_type === "post" ){
		
		if(isset($_POST['TravelAds_CheckAuto']) && $_POST['TravelAds_CheckAuto'] == 'yes'){
		$Query="INSERT INTO `wp_tan_pages_config`(`page_id`, `mode`, `link_amount`, `update_links`) VALUES ('".$post_id."','0','3','1')
		ON DUPLICATE KEY UPDATE
			`mode`='0', 
			`link_amount`='3', 
			`update_links`='1'
		";
		}else{
		$Query="INSERT INTO `wp_tan_pages_config`(`page_id`, `mode`, `link_amount`, `update_links`) VALUES ('".$post_id."','1','3','1')
		ON DUPLICATE KEY UPDATE
			`mode`='1', 
			`link_amount`='3', 
			`update_links`='1'
		";
		}//End if

		$wpdb->query($Query);
	}//End if
});
