<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class traveladsConfig {
	private $config = null;
	static private $instance = null;

	//Singleton: private construct
	private function __construct() {
		if( is_admin() ) {
			//Add the admin page and settings
			add_action('admin_menu',array($this,'addmenu'));
		}
	}
	
	static public function instance() {
		//Only one instance
		if (self::$instance == null) {
			self::$instance = new traveladsConfig();
		}
		
		return self::$instance;
		}
	
	public function show() {
?>

<div class="wrap">

	<div id="travelads_main">

		<?php 

		global $wpdb;

		//
		// Test HO ID
		//
		if(isset($_COOKIE['TAN_hoID']))
		{
			$res = $wpdb->get_results("SELECT COUNT(*) as 'res' FROM `wp_options` WHERE `option_name` = 'TAN_HO_ID'");
			if($res[0]->{'res'} === '1')
				$res = $wpdb->query("DELETE FROM `wp_options` WHERE `option_name` = 'TAN_HO_ID'");

			$wpdb->query("INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) 
						VALUES (NULL, 'TAN_HO_ID', '".$_COOKIE['TAN_hoID']."', 'yes');");
		}

		//
		// Test login cookie
		//
		$code = tan_test_log();

		if($code != 202):	// wrong cookie, we display the login form
		?>

<div id="login_window">
	<p class="title"><?php echo "<img src=".plugins_url("assets/images/logo.png", dirname(__FILE__)).">"; ?>TravelAdsNetwork</p>
	<p class="subtitle">The Travel Ads Network for Travel Bloggers</p>
	<form id='TANLoginForm'>
		<div class="field">
			<label class="label">Username or E-mail</label>
			<div class="control">
				<input class="input" name="username" type="text" placeholder="Your Username or E-mail">
			</div>
		</div>
		<div class="field">
			<label class="label">Password</label>
			<div class="control">
				<input class="input" name="password" type="password" value="" placeholder="Your password">
			</div>
		</div>
		<div class="field" id=wrongCreds>
			<div class="control">
				Wrong credentials!
			</div>
		</div>
		<div class="field is-grouped">
			<div class="control">
				<p>
					Don’t have an account? <a href="https://traveladsnetwork.com/register/" target="_blank">Sign up</a> today!  
				</p>
			</div>
		</div>
		<div class="field">
			<div class="control">					
				<button type='submit'  class="button is-link" id="checkLog" style="padding-top: 0;padding-bottom: 0;margin-top: 0;display: block;border-radius: 25px;padding-left: 35px;padding-right: 35px;">Submit</button>
			</div>
		</div>
	</form>
</div>

<?php
		else: //cookie ok, we display the configuration plugin

			// Do we have to update the database?
			if(isset($_COOKIE['TAN_update']))
			{
				$obj = json_decode(stripslashes($_COOKIE['TAN_update']));

				if(is_object($obj))
					foreach($obj->{'pages'} as $page)
					{
						$query = "INSERT INTO `wp_tan_pages_config` (`page_id`, 
														`mode`,
														`link_amount`,
														`update_links`,
														`merchants`) 
								VALUES ('".$page."',
										'".$obj->{'mode'}."',
										'".$obj->{'linkAmount'}."',
										'".$obj->{'updateLinks'}."',
										'".json_encode($obj->{'merchants'})."')
								ON DUPLICATE KEY UPDATE
									`mode`='".$obj->{'mode'}."', 
									`link_amount`='".$obj->{'linkAmount'}."', 
									`update_links`='".$obj->{'updateLinks'}."',
									`merchants`='".json_encode($obj->{'merchants'})."'";

						$wpdb->query($query);
					}
				if($_COOKIE['TAN_update'] !== '')
				{
					?>
						<div id=savedDiv>
							Your new configuration has been successfully saved!
						</div>
					<?php
				}
			}
?>

<hr>
<!-- Html -->
<div id='content'>
	<table id=topTable>
		<tr>
			<td>
				<h1>
				<?php _e('<img src="'.plugins_url("assets/images/logo.png", dirname(__FILE__)).'"> TravelAdsNetwork Settings','travelads'); ?>
				</h1>
			<td>
			<td><button class='saveChangesButton button button-primary'>Apply changes</button></td>
		</tr>
	</table>

	<form id=configForm>
		<h3>General Settings</h3>
		<table class=generalSettings>
			<tr>
				<td>Mode:</td>
				<td id="radioMode">
					<input id=autoMode type='radio' name='mode' value="auto" checked>
						<label for=autoMode>Automatic - Let our application do the job for you</label><br>
					<input id=manualMode type='radio' name='mode' value="manual">
						<label for=manualMode>Manual</label>
				</td>
			</tr>
		</table>
		<div id=soloAuto>
			<table class=generalSettings>
				<tr>
					<td>Maximum amount of links per page:</td>
					<td>
						<input type="text" id=linkAmount value="3" style="width: 50px;">
					</td>
				</tr>
				<tr>
					<td>Update existing links:</td>
					<td>
						<input id=yesUpdate type='radio' name='updateLinks' value="manual" checked>
							<label for=yesUpdate>Yes</label>
						<input id=noUpdate type='radio' name='updateLinks' value="auto">
							<label for=noUpdate>No</label><br>
					</td>
				</tr>
			</table>
		</div>
			<h3>Offer filters</h3>
			<table id=filtersTable>
				<tr>
					<td>
						<input type=text id=searchMerchants placeholder='Search your merchant'></input>
						<ul id=merchantList>
							<?php
								foreach(json_decode(file_get_contents(plugins_url("data/advertisers.json", dirname(__FILE__)))) as $a)
									echo '<li value="'.$a->{'id'}.'"> '.$a->{'name'};
							?>
						</ul>
					</td>
					<td id=selectedMerchantCell>
						<h4>Select your favorite merchants:</h4>
						<ul id=selectedMerchantList class=grid>
						</ul>
					</td>
				</tr>
			</table>
		<h3>Apply to your Post and Pages:</h3>
		<div id='pagesList'>
			<input id=pageSearch type=text placeholder='Search your page'>
			<button type=button id=pageSelectAll class=button>select all</button>
			<button type=button id=pageSelectNone class=button>deselect all</button>
			<ul>
				<?php
				foreach(get_pages() as $page)
					echo '<li value="'.$page->{'ID'}.'">'.$page->{'post_title'};
				foreach(get_posts() as $post)
					echo '<li value="'.$post->{'ID'}.'">'.$post->{'post_title'};
				?>
			</ul>
		</div>
	</form>
	<button class='saveChangesButton button button-primary'>Apply changes</button>
</div>

<?php
		endif; 
	}

	public function addmenu() {
		$hook=add_menu_page(__('TravelAdsNetwork Settings','travelads'),'TravelAdsNetwork','manage_options','travelads',array($this,'show'),plugins_url("assets/images/logo.png", dirname(__FILE__)),3);
	}

}
