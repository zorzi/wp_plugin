<?php
/*
Plugin Name: TravelAdsNetwork.com
Description: Auto detect the keywords in your page to add links
Version: 1.0
Author: TravelAdsNetwork
Author URI: https://traveladsnetwork.com
*/

$tan_log_code=-1;

function tan_test_log() //USE THIS FUNCTION TO TEST IF THE USER IS LOGGED IN TAN
{
	global $tan_log_code;
	if($tan_log_code !== -1)
		return $tan_log_code;

	if(isset($_COOKIE['TAN_login']))
	{
		$request = new WP_Http;

		// 
		// even if the user modifies the cookies, no problems, it's 
		// treated on the api side
		//
		
		$username = isset($_COOKIE['TAN_username'])?$_COOKIE['TAN_username']:'';
		$nonce = isset($_COOKIE['TAN_login'])?$_COOKIE['TAN_login']:'';
		$result = $request->get('https://traveladsnetwork.com/api-remote-login/?method=checkNonce'
					.'&username='.$username
					.'&nonce='.$nonce);

		$tan_log_code = $result['response']['code'];
		
		if($tan_log_code === 202)
		{
			wp_enqueue_script("traveladsnetwork_updatecookiejs", plugins_url('assets/js/updateCookie.js', __FILE__));
			wp_localize_script( 'traveladsnetwork_updatecookiejs', 'new_cookie', $result['body']);
		}
	}
	else
		$tan_log_code = 400;

	return $tan_log_code;
}

define('travelads_PLUGIN_DIR',plugin_dir_path(__FILE__));

add_action('admin_print_styles', function() {
	switch(preg_replace('/\?.*$/', '', $_SERVER['REQUEST_URI']))
	{
		case('/wp-admin/admin.php'):
			if(tan_test_log() === 202)
				wp_enqueue_style("traveladsnetwork_configcss", plugins_url('assets/css/config.css', __FILE__));
			else
				wp_enqueue_style("traveladsnetwork_logincss", plugins_url('assets/css/login.css', __FILE__));
			break;
		case('/wp-admin/post.php'):
			wp_enqueue_style("traveladsnetwork_pageEditcss", plugins_url('assets/css/pageEdit.css', __FILE__));
			break;
	}
});
add_action('admin_print_scripts', function() {
	tan_test_log();
	switch(preg_replace('/\?.*$/', '', $_SERVER['REQUEST_URI']))
	{
		case('/wp-admin/admin.php'):
			if(tan_test_log() === 202)
			{
				wp_enqueue_script("traveladsnetwork_dragNDropjs", plugins_url('assets/js/DnD.js', __FILE__));
				wp_enqueue_script("traveladsnetwork_configjs", plugins_url('assets/js/config.js', __FILE__));
				wp_localize_script( 'traveladsnetwork_configjs', 'php_data', isset($_COOKIE['TAN_savedConfig'])?stripslashes($_COOKIE['TAN_savedConfig']):'null');

			}
			else
				wp_enqueue_script("traveladsnetwork_loginjs", plugins_url('assets/js/login.js', __FILE__));
			break;
		case('/wp-admin/post.php'):
			wp_enqueue_script("traveladsnetwork_pageEditjs", plugins_url('assets/js/pageEdit.js', __FILE__));
			break;
	}
});

add_action('wp_enqueue_scripts', function() {
	wp_enqueue_script('jquery');
	global $wpdb;
	$ID = get_the_ID();
	$res = $wpdb->get_results('SELECT * FROM wp_tan_pages_config
				WHERE `page_id` = "'.$ID.'"');

	$affiliate_id = get_option('TAN_HO_ID');

	if($affiliate_id !== false && count($res) > 0):
		$pageConfig = $res[0];

		if (isset($_COOKIE["TAN_matches"])) 
		{
			$matches = json_decode(stripslashes($_COOKIE["TAN_matches"])); 
			foreach($matches as $key => $match)
				$wpdb->query("INSERT INTO `wp_tan_keywords` (`keyword`, `links`) 
							VALUES (LOWER('".$key."'), '".json_encode($match)."')");
		}

		$res = $wpdb->get_results("SELECT * FROM `wp_tan_keywords`");
		$savedMatches = [];

		foreach($res as $r)
			$savedMatches += [$r->{'keyword'} => json_decode($r->{'links'})];
		
		if($pageConfig->{'mode'} === '1')	//automatic mode
		{
			wp_enqueue_script("traveladsnetwork_autoModejs", plugins_url('assets/js/autoMode.js', __FILE__));
			wp_localize_script('traveladsnetwork_autoModejs',
			       		'php_data',
					['savedMatches' => $savedMatches ,
					 'affiliate_id' => $affiliate_id,
					 'pageConfig' => $pageConfig]);
		}
		else	// manual mode
		{
			wp_enqueue_script("traveladsnetwork_manualModejs", plugins_url('assets/js/manualMode.js', __FILE__));
			wp_localize_script('traveladsnetwork_manualModejs',
			       		'php_data',
					['savedMatches' => $savedMatches ,
					 'affiliate_id' => $affiliate_id]);
		}
	endif;
});

// Load config class
include(travelads_PLUGIN_DIR.'classes/traveladsConfig.php');

$tra_mode = isset($_COOKIE['TAN_savedConfig'])?stripslashes($_COOKIE['TAN_savedConfig']):'null';
// Check Manual from Cookie
if(isset(json_decode($tra_mode)->{'mode'}) && json_decode($tra_mode)->{'mode'} == '0'){
	require_once(travelads_PLUGIN_DIR.'classes/traveladsBox.php');
	require_once(travelads_PLUGIN_DIR.'classes/traveladsButton.php');
}//End if

add_action( 'init',function(){
});

// Initialize the cache at least once
$tan_object = traveladsConfig::instance();

add_action( 'delete_post', function($id) {
	global $wpdb;
	$wpdb->query("DELETE FROM `wp_tan_pages_config` WHERE `page_id` = '$id'");
});


add_action('save_post', function ($id) {
	// If this is just a revision, don't send the email.
	if ( wp_is_post_revision( $post_id ) )
		return;

	global $wpdb;
	$wpdb->query("INSERT INTO `wp_tan_pages_config` (`page_id`) VALUES '$post_id'");
});

register_uninstall_hook( __FILE__, 'tan_remove');
function tan_remove() {
	global $wpdb;

	$wpdb->query("DROP TABLE `wp_tan_pages_config`");
	$wpdb->query("DROP TABLE `wp_tan_keywords`");
}

register_deactivation_hook( __FILE__, function() {
});

register_activation_hook( __FILE__, function () {
	global $wpdb;

	$wpdb->query("CREATE TABLE `wp_tan_pages_config` ( `page_id` INT NOT NULL ,
								`mode` BOOLEAN NOT NULL DEFAULT TRUE , 
								`link_amount` INT NOT NULL DEFAULT '3' , 
								`update_links` BOOLEAN NOT NULL DEFAULT TRUE ,
								`merchants` VARCHAR(128) NOT NULL DEFAULT '[]' , 
								PRIMARY KEY (`page_id`))");

	$insertQuery='INSERT INTO `wp_tan_pages_config` (`page_id`) VALUES';

	$idList=[];
	if(is_object(get_pages()))
		foreach(get_pages() as $page)
			array_push($idList, $page->{'ID'});
	if(is_object(get_posts()))
		foreach(get_posts() as $post)
			array_push($idList, $post->{'ID'});

	foreach($idList as $id)
		$insertQuery .= '("'.$id.'"), ';

	$insertQuery = trim($insertQuery, ', ');

	$wpdb->query($insertQuery);

	$wpdb->query('CREATE TABLE `wp_tan_keywords` ( `keyword` VARCHAR(128) NOT NULL , 
									`links` VARCHAR(2048) NOT NULL , 
									PRIMARY KEY (`keyword`))');
});

// Do not pollute other plugins
unset($tan_object);
